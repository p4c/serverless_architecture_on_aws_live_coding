export const environment = {
  production: false,
  REGION: 'eu-west-1',
  SERVER_API_URL: 'https://wvvuodg7bd.execute-api.eu-west-1.amazonaws.com/production',
  COGNITO_USERPOOL_ID: 'eu-west-1_yMlkYs89i',
  COGNITO_CLIENT_ID: '1cn3m3q2kdn4l19q3djo8mdbbm',
  COGNITO_DOMAIN_PREFIX: 'person',
};
