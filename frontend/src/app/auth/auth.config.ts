import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from '../../environments/environment';

export const authConfig: AuthConfig = {
  scope: 'openid profile email',
  issuer: 'https://cognito-idp.' + environment.REGION + '.amazonaws.com/' + environment.COGNITO_USERPOOL_ID,
  redirectUri: window.location.origin,
  clientId: environment.COGNITO_CLIENT_ID,
  responseType: 'token',
  strictDiscoveryDocumentValidation: false,
  logoutUrl: 'https://' + environment.COGNITO_DOMAIN_PREFIX + '.auth.' + environment.REGION + '.amazoncognito.com/logout' +
                '?client_id=' + environment.COGNITO_CLIENT_ID + '&logout_uri=' + encodeURIComponent(window.location.origin)
};
