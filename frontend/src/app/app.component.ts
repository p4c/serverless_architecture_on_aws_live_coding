import { Component } from '@angular/core';
import { OAuthService, NullValidationHandler } from 'angular-oauth2-oidc';
import { authConfig } from './auth/auth.config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'person-app';

  constructor(private oauthService: OAuthService) {
    this.configure();
  }

  login() {
    this.oauthService.initImplicitFlow();
  }

  logout() {
    this.oauthService.logOut();
  }

  public get isAuthenticated() {
    return this.oauthService.hasValidAccessToken();
  }

  public get name() {
    const claims = this.oauthService.getIdentityClaims();
    if (claims) {
      return claims['given_name'] + ' ' + claims['family_name'];
    }
    return null;
  }

  private configure() {
    console.log(authConfig);
    this.oauthService.configure(authConfig);
    this.oauthService.setStorage(localStorage);
    this.oauthService.tokenValidationHandler = new NullValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
    this.oauthService.tryLogin();
  }
}
