export class Person {
    id: string;
    name: string;
}

export class PersonList {
    persons: Person[];
    total: number;
    count: number;
}
