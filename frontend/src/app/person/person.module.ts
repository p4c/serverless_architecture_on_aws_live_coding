import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PersonCreateComponent } from './person-create/person-create.component';
import { PersonListComponent } from './person-list/person-list.component';
import { PersonService } from './person.service';
import { personRoutes } from './person.routes';

@NgModule({
  declarations: [
    PersonCreateComponent,
    PersonListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(personRoutes)
  ],
  providers: [
    PersonService
  ]
})
export class PersonModule { }
