import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PersonService } from '../person.service';
import { Person } from '../person';

@Component({
  selector: 'app-person-create',
  templateUrl: './person-create.component.html',
  styleUrls: ['./person-create.component.css']
})
export class PersonCreateComponent {
  person: Person = new Person();

  constructor(private personService: PersonService, private router: Router) { }

  create() {
    this.personService.create(this.person).subscribe((person) => {
      this.router.navigate(['/person/list']);
    },
    (error) => {
      console.error(error);
    });
  }
}
