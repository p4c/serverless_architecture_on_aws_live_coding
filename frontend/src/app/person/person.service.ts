import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Person, PersonList } from './person';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = environment.SERVER_API_URL + '/person';

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  constructor(private http: HttpClient) { }

  list(): Observable<PersonList> {
    return this.http.get<PersonList>(apiUrl);
  }

  create(person: Person): Observable<Person> {
    return this.http.post<Person>(apiUrl, person, httpOptions).pipe(
      tap((p: Person) => console.log(`added person w/ id=${p.id}`)),
      catchError(this.handleError<Person>('createPerson'))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
