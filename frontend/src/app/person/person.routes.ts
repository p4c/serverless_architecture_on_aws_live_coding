import { Routes } from '@angular/router';

import { PersonListComponent } from './person-list/person-list.component';
import { PersonCreateComponent } from './person-create/person-create.component';
import { AuthGuardService } from '../auth/auth-guard.service';

export const personRoutes: Routes = [
  {
    path: 'person',
    canActivate: [AuthGuardService],
    children: [
      { path: '', pathMatch: 'prefix', redirectTo: 'list' },
      { path: 'list', component: PersonListComponent },
      { path: 'create', component: PersonCreateComponent }
    ]
  }
];
