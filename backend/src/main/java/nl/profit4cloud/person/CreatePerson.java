package nl.profit4cloud.person;

import java.util.HashMap;
import java.util.UUID;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import org.apache.http.HttpStatus;

public class CreatePerson implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    final String TABLE_NAME = System.getenv("TABLE_NAME");

	final AmazonDynamoDB dynamoDBClient = AmazonDynamoDBClientBuilder.defaultClient();
	final DynamoDB dynamoDB = new DynamoDB(dynamoDBClient);

	@SuppressWarnings("serial")
	@Override
	public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent request, Context context) {

		// INITIALIZE RESPONSE
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
		response.setHeaders(new HashMap<String, String>() {{
			put("Access-Control-Allow-Origin", "*");
		}});

		try {
			UUID uuid = UUID.randomUUID();
			
	        Table table = dynamoDB.getTable(TABLE_NAME);

			Item item = Item.fromJSON(request.getBody());
			table.putItem(item.withPrimaryKey("id", uuid.toString()));

			response.setStatusCode(HttpStatus.SC_CREATED);
			response.setBody(item.toJSON());

		} catch (Exception e) {
			response.setStatusCode(HttpStatus.SC_BAD_REQUEST);
			response.setBody(e.getMessage());
		}
		return response;
	}
}
