package nl.profit4cloud.person;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;

import org.apache.http.HttpStatus;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ListPerson implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    final String TABLE_NAME = System.getenv("TABLE_NAME");

	final AmazonDynamoDB dynamoDBClient = AmazonDynamoDBClientBuilder.defaultClient();
	final DynamoDB dynamoDB = new DynamoDB(dynamoDBClient);

	@SuppressWarnings({ "serial", "unchecked" })
	@Override
	public APIGatewayProxyResponseEvent handleRequest(APIGatewayProxyRequestEvent request, Context context) {
		
		// INITIALIZE RESPONSE
		APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();
		response.setHeaders(new HashMap<String, String>() {{
			put("Access-Control-Allow-Origin", "*");
		}});

		try {
			ScanRequest scanRequest = new ScanRequest().withTableName(TABLE_NAME);
			ScanResult result = dynamoDBClient.scan(scanRequest);

			JSONObject body = new JSONObject();
			body.put("count", result.getCount());
			body.put("total", result.getScannedCount());

			JSONArray persons = new JSONArray();
            for (Map<String, AttributeValue> item : result.getItems()) {
            	JSONObject person = new JSONObject();
            	item.entrySet().stream().forEach(entry -> {
            		person.put(entry.getKey(), entry.getValue().getS());
            	});
            	persons.add(person);
            }
            body.put("persons", persons);
            
			response.setStatusCode(HttpStatus.SC_OK);
			response.setBody(body.toJSONString());

		} catch (Exception e) {
			response.setStatusCode(HttpStatus.SC_BAD_REQUEST);
			response.setBody(e.getMessage());
		}
		return response;
	}
}
